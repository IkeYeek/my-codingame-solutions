import copy
from typing import List

operation: str = input()
pseudo_random_number = int(input())
rotors: List[str] = [input(), input(), input()]
message: str = input()


def caesar(starting_shift: int, unshifted: str) -> str:
    shifted = str()
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for _i, c in enumerate(unshifted):
        position = (alphabet.index(c) + (starting_shift + _i)) % 26
        shifted += alphabet[position]

    return shifted


def brutus(starting_shift: int, shifted: str) -> str:
    unshifted = str()
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for i in range(len(shifted)-1, -1, -1):
        c = shifted[i]
        position = (alphabet.index(c) - (starting_shift + i)) % 26
        unshifted += alphabet[position]

    return unshifted[::-1]


def encode(raw: str, _rotors: List[str]):
    curr = copy.copy(raw)
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for _rotor in _rotors:
        building = ""
        for _i, _c in enumerate(curr):
            position_alphabet = alphabet.index(_c)
            building += _rotor[position_alphabet]
        curr = building
    return curr


def decode(raw: str, _rotors: List[str]):
    curr = copy.copy(raw)
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for _rotor in reversed(_rotors):
        building = ""
        for _i, _c in enumerate(curr):
            position_rotor = _rotor.index(_c)
            building += alphabet[position_rotor]
        curr = building
    return curr


print(encode(caesar(pseudo_random_number, message), rotors)) if operation == "ENCODE" else \
    print(brutus(pseudo_random_number, decode(message, rotors)))