from typing import Dict, List
from copy import copy
# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
mode = None
if mode is None:
    n = int(input())
    rods: List[List[int]] = [[] for _ in range(n+1)]
    for i in range(n):
        a, b = [int(j) for j in input().split()]
        rods[a].append(b)
        rods[b].append(a)
else:
    with open(mode) as f:
        n = int(f.readline())
        rods: List[List[int]] = [[] for _ in range(n+1)]
        for i in range(n):
            a, b = [int(j) for j in f.readline().split()]
            rods[a].append(b)
            rods[b].append(a)

def min_complete_from(padi: int, graph, currmin) -> int:
    """
    Méthode utilisée pour parcourir le graphe:
    - On initialise voisins [] en copiant les keys du graph
    - On enlève le point courant de la liste
    - On initialise "total_nights" à 0
    - On initialise next_neighbors à [padi]
    - Tantque il reste des voisins
        - Pour chaque point de next_neighbors:
            - Si le point n'a pas été visité, et que total_nights n'a pas encore été incrémenté dans ce tours, on l'incrémente.
            - Si le point n'a' pas été visit, on l'ajoute dans next_neighbors

    :param padi: l'index du pad depuis lequel on veut commencer le parcours du graphe
    :param graph: Le graph à visiter
    :return: le nombre minimal de nuits nécessaires au parcours complet du graphe
    """
    left_points = {j for j in range(len(graph))}
    left_points.remove(padi)
    total_nights = 0
    next_neighbors = [padi]
    while len(left_points) > 0:
        curr_neighbors = copy(next_neighbors)
        next_neighbors = []
        incremented = False

        for curr_neighbor in curr_neighbors:
            if not incremented:
                total_nights += 1
                incremented = True
                if currmin >= 0 and total_nights > currmin:
                    return currmin+1
            for neighbor in graph[curr_neighbor]:
                if neighbor in left_points:
                    next_neighbors.append(neighbor)
            if curr_neighbor in left_points:
                left_points.remove(curr_neighbor)
    return total_nights - 1

def find_optimal_start(graph):
    minl = None
    index = None
    for node in graph:
        node = node[0]
        if minl is None:
            minl = min_complete_from(node, graph, -1)
            index = node
        else:
            cv = min_complete_from(node, graph, minl)
            if cv < minl:
                minl = cv
                index = node
    return minl

print(find_optimal_start(rods))