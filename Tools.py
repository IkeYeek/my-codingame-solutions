from __future__ import annotations

import time
from enum import Enum
from typing import Dict, Tuple, List

from rich.console import Console


class LEVEL(Enum):
    NORMAL = 1
    ERROR = 2
    DEBUG = 3
    VERBOSE = 4


class Lol4j:
    instance: Lol4j | None = None

    def __init__(self, level: LEVEL = LEVEL.ERROR):
        self.level = level
        self.console = Console()

    @staticmethod
    def set_level_i(level: LEVEL):
        Lol4j.getinstance().set_level(level)

    def set_level(self, level: LEVEL):
        self.level = level

    @staticmethod
    def getinstance() -> Lol4j:
        if Lol4j.instance is None:
            Lol4j.instance = Lol4j()
        return Lol4j.instance

    def log_debug(self, msg, emitter="unknown"):
        self.log_l(LEVEL.DEBUG, msg, emitter)

    def log_error(self, msg, emitter="unknows"):
        self.log_l(LEVEL.ERROR, msg, emitter)

    def log(self, msg, emitter="unknown"):
        self.log_l(LEVEL.NORMAL, msg, emitter)

    def log_l(self, level: LEVEL, msg: str, emitter: str = "unknown"):
        if level.value <= self.level.value:
            level = "DEBUG" if level == LEVEL.DEBUG else "VERBOSE" if level == LEVEL.VERBOSE else "ERROR" if level == LEVEL.ERROR else "NORMAL"
            style = "bold orange" if level == LEVEL.DEBUG else "bold white" if level == LEVEL.VERBOSE else "bold red" if level == LEVEL.ERROR else "bold white"
            self.console.print("[%s] FROM %s: %s" % (level, msg, emitter), style=style)


class Benchmarker:
    _INSTANCE = None
    _logger = Lol4j().getinstance()

    @staticmethod
    def get_instance() -> Benchmarker:
        if Benchmarker._INSTANCE is None:
            Benchmarker._INSTANCE = Benchmarker()
        return Benchmarker._INSTANCE

    def __init__(self):
        self.benchmarks: Dict[int, Tuple[float, float, str]] = {}

    def start(self, n: int, f: str = "Unknown Source"):
        if n not in self.benchmarks:
            self.benchmarks[n] = (time.time_ns(), -1, f)

    def end(self, n: int, autolog: bool = True):
        if n in self.benchmarks:
            self.benchmarks[n] = (self.benchmarks[n][0], time.time_ns(), self.benchmarks[n][2])
            if autolog:
                self.log(n)

    def log(self, n: int):
        if n in self.benchmarks and len(self.benchmarks[n]) == 3:
            bench_n = self.benchmarks[n]
            dur = bench_n[1] - bench_n[0]
            dur_s = dur * 1e-9
            Benchmarker._logger.log_debug("Benchmark[%s] - %fns - %fs" % (bench_n[2], dur, dur_s), "Benchmarker")


if __name__ == '__main__':
    with open("medium/plague") as f:
        n = int(f.readline())
        rods: List[List[int]] = [[] for _ in range(n+1)]
        for i in range(n):
            a, b = [int(j) for j in f.readline().split()]
            rods[a].append(b)
            rods[b].append(a)
        log = Lol4j.getinstance()
        log.set_level(LEVEL.DEBUG)
        bm = Benchmarker.get_instance()
        left_points = set()
        bm.start(0, "set")
        left_points = {j for j in range(9999999)}
        bm.end(0)
        left_points = list()
        bm.start(1, "list")
        for j in range(9999999):
            left_points.append(j)
        bm.end(1)
